package com.xaffax.jpainheritance;

import com.xaffax.jpainheritance.model.AbstractTableChildOne;
import com.xaffax.jpainheritance.model.AbstractTableChildTwo;
import com.xaffax.jpainheritance.model.ConcreteTableOneFromChildOne;
import com.xaffax.jpainheritance.model.ConcreteTableOneFromChildTwo;
import com.xaffax.jpainheritance.model.ConcreteTableTwoFromChildOne;
import com.xaffax.jpainheritance.model.ConcreteTableTwoFromChildTwo;
import com.xaffax.jpainheritance.repository.ChildOneRepository;
import com.xaffax.jpainheritance.repository.ChildTwoRepository;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JpaInheritanceApplicationTests {

    @Autowired
    private ChildOneRepository childOneRepository;

    @Autowired
    private ChildTwoRepository childTwoRepository;

    @Test
    public void aTableLoadTest() {
        List<ConcreteTableOneFromChildOne> concreteTableOneFromChildOne;
        List<ConcreteTableTwoFromChildOne> concreteTableTwoFromChildOne;
        List<ConcreteTableOneFromChildTwo> concreteTableOneFromChildTwo;
        List<ConcreteTableTwoFromChildTwo> concreteTableTwoFromChildTwo;

        concreteTableOneFromChildOne = Stream.generate(ConcreteTableOneFromChildOne::new).limit(20).map(e -> {
            e.generateRandomFields();
            return e;
        }).collect(Collectors.toList());

        concreteTableOneFromChildTwo = Stream.generate(ConcreteTableOneFromChildTwo::new).limit(20).map(e -> {
            e.generateRandomFields();
            return e;
        }).collect(Collectors.toList());

        concreteTableTwoFromChildOne = Stream.generate(ConcreteTableTwoFromChildOne::new).limit(20).map(e -> {
            e.generateRandomFields();
            return e;
        }).collect(Collectors.toList());

        concreteTableTwoFromChildTwo = Stream.generate(ConcreteTableTwoFromChildTwo::new).limit(20).map(e -> {
            e.generateRandomFields();
            return e;
        }).collect(Collectors.toList());

        childOneRepository.saveAll(concreteTableOneFromChildOne);
        childOneRepository.saveAll(concreteTableTwoFromChildOne);
        childTwoRepository.saveAll(concreteTableOneFromChildTwo);
        childTwoRepository.saveAll(concreteTableTwoFromChildTwo);
    }

    @Test
    public void bReadTableTest() {
        Iterable<AbstractTableChildOne> elementsAbstractTableChildOne = childOneRepository.findAll();
        Iterable<AbstractTableChildTwo> elementsAbstractTableChildTwo = childTwoRepository.findAll();

        System.out.println("========= Starting AbstractTableChildOne =========");
        elementsAbstractTableChildOne.iterator().forEachRemaining(e -> {
            if (e instanceof ConcreteTableOneFromChildOne) {
                System.out.println("This is ConcreteTableOneFromChildOne element: " + e);
            } else {
                System.out.println("This is ConcreteTableTwoFromChildOne element: " + e);
            }
        });
        System.out.println("========= Starting AbstractTableChildTwo =========");
        elementsAbstractTableChildTwo.iterator().forEachRemaining(e -> {
            if (e instanceof ConcreteTableOneFromChildTwo) {
                System.out.println("This is ConcreteTableOneFromChildTwo element: " + e);
            } else {
                System.out.println("This is ConcreteTableTwoFromChildTwo element: " + e);
            }
        });
    }
}
