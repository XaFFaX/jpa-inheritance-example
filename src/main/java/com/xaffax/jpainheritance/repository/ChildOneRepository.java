package com.xaffax.jpainheritance.repository;

import com.xaffax.jpainheritance.model.AbstractTableChildOne;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildOneRepository extends BaseRepository<AbstractTableChildOne> {

    AbstractTableChildOne findByAbstractChildOneParameterOne(Integer abstractChildOneParameterOne);
}