package com.xaffax.jpainheritance.repository;

import com.xaffax.jpainheritance.model.AbstractTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T extends AbstractTable> extends CrudRepository<T, Long> {

    T findByAbstractTableParameter(Integer abstractTableParameter);
}