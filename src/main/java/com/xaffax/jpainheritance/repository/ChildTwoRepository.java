package com.xaffax.jpainheritance.repository;

import com.xaffax.jpainheritance.model.AbstractTableChildTwo;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildTwoRepository extends BaseRepository<AbstractTableChildTwo> {

    AbstractTableChildTwo findByAbstractChildTwoParameterOne(Integer abstractChildTwoParameterOne);
}