package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//@MappedSuperclass
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "abstractTableParameter")
    private Integer abstractTableParameter;

    @Column(name = "abstractTableDate")
    @Temporal(TemporalType.DATE)
    private Date abstractTableDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAbstractTableParameter() {
        return abstractTableParameter;
    }

    public void setAbstractTableParameter(Integer abstractTableParameter) {
        this.abstractTableParameter = abstractTableParameter;
    }

    public Date getAbstractTableDate() {
        return abstractTableDate;
    }

    public void setAbstractTableDate(Date abstractTableDate) {
        this.abstractTableDate = abstractTableDate;
    }

    @Override
    public String toString() {
        return "AbstractTable{" +
                "id=" + id +
                ", abstractTableParameter=" + abstractTableParameter +
                ", abstractTableDate=" + abstractTableDate +
                '}';
    }

    public AbstractTable generateRandomFields() {
        Random random = new Random();
        this.abstractTableParameter = random.nextInt();
        this.abstractTableDate = new Date(random.nextInt());
        return this;
    }
}
