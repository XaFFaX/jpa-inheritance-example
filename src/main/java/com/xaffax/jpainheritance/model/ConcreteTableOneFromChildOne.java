package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tableOneFromChildOne")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ConcreteTableOneFromChildOne extends AbstractTableChildOne implements Serializable {

}
