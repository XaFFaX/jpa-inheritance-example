package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import java.util.Random;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

//@MappedSuperclass
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTableChildOne extends AbstractTable implements Serializable {

    @Column(name = "abstractChildOneParameterOne")
    private Integer abstractChildOneParameterOne;

    @Column(name = "abstractChildOneParameterTwo")
    private Integer abstractChildOneParameterTwo;

    public Integer getAbstractChildOneParameterOne() {
        return abstractChildOneParameterOne;
    }

    public void setAbstractChildOneParameterOne(Integer abstractChildOneParameterOne) {
        this.abstractChildOneParameterOne = abstractChildOneParameterOne;
    }

    public Integer getAbstractChildOneParameterTwo() {
        return abstractChildOneParameterTwo;
    }

    public void setAbstractChildOneParameterTwo(Integer abstractChildOneParameterTwo) {
        this.abstractChildOneParameterTwo = abstractChildOneParameterTwo;
    }

    @Override
    public String toString() {
        return super.toString() + "\nAbstractTableChildOne{" +
            "abstractChildOneParameterOne=" + abstractChildOneParameterOne +
            ", abstractChildOneParameterTwo=" + abstractChildOneParameterTwo +
            '}';
    }

    @Override
    public AbstractTableChildOne generateRandomFields()
    {
        super.generateRandomFields();
        Random random=new Random();
        this.setAbstractChildOneParameterOne(random.nextInt());
        this.setAbstractChildOneParameterTwo(random.nextInt());
        return this;
    }
}
