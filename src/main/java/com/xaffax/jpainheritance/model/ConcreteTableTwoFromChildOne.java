package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tableTwoFromChildOne")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ConcreteTableTwoFromChildOne extends AbstractTableChildOne implements Serializable {

}
