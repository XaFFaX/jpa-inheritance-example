package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import java.util.Random;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

//@MappedSuperclass
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTableChildTwo extends AbstractTable implements Serializable {

    @Column(name = "abstractChildTwoParameterOne")
    private Integer abstractChildTwoParameterOne;

    @Column(name = "abstractChildTwoParameterTwo")
    private Integer abstractChildTwoParameterTwo;

    public Integer getAbstractChildTwoParameterOne() {
        return abstractChildTwoParameterOne;
    }

    public void setAbstractChildTwoParameterOne(Integer abstractChildTwoParameterOne) {
        this.abstractChildTwoParameterOne = abstractChildTwoParameterOne;
    }

    public Integer getAbstractChildTwoParameterTwo() {
        return abstractChildTwoParameterTwo;
    }

    public void setAbstractChildTwoParameterTwo(Integer abstractChildTwoParameterTwo) {
        this.abstractChildTwoParameterTwo = abstractChildTwoParameterTwo;
    }

    @Override
    public String toString() {
        return super.toString() + "\nAbstractTableChildOne{" +
                "abstractChildTwoParameterOne=" + abstractChildTwoParameterOne +
                ", abstractChildTwoParameterTwo=" + abstractChildTwoParameterTwo +
                '}';
    }

    @Override
    public AbstractTableChildTwo generateRandomFields() {
        super.generateRandomFields();
        Random random = new Random();
        this.setAbstractChildTwoParameterOne(random.nextInt());
        this.setAbstractChildTwoParameterTwo(random.nextInt());
        return this;
    }
}
