package com.xaffax.jpainheritance.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tableOneFromChildTwo")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ConcreteTableOneFromChildTwo extends AbstractTableChildTwo implements Serializable {

}
